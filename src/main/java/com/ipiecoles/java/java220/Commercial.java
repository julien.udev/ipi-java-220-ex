package com.ipiecoles.java.java220;

public class Commercial extends Employe {

	private Double caAnnuel;

	public Double getCaAnnuel() {
		return caAnnuel;
	}

	public void setCaAnnuel(Double caAnnuel) {
		this.caAnnuel = caAnnuel;
	}

	@Override
	public Double getPrimeAnnuelle() {
		Double prime = 500d;
		if (caAnnuel > 1000) {
			prime =0.5*caAnnuel; 
		}
//		else {
//			prime = 0.5*caAnnuel;
//		}
//		return prime;
		return prime;
	}

}
